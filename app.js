var assert = require('assert');
var async = require('async');
var express = require('express');
var http = require('http');
var bodyParser  = require('body-parser');
var MongoClient = require('mongodb').MongoClient;
var ObjectId = require('mongodb').ObjectID;
var multer = require('multer');
var upload = multer({
        dest : 'uploads/'
    });
var csv = require('csv-parse');
var fs = require('fs');
var app = express();
var server = http.createServer(app);

// parse application/x-www-form-urlencoded
app.use(bodyParser.urlencoded({ extended: false }))
// parse application/json
app.use(bodyParser.json())

// Connection URL
var db_connection_string = fs.readFileSync('config/.db-connection', 'utf8');;

server.listen(7777, function(){
   console.log('server start to listen port 7777');
});

// api route
app.use(function (req, res, next) {
    var ip = (req.headers['x-forwarded-for'] || '').split(',')[0] 
        || req.connection.remoteAddress;
    console.log(ip);
 
    res.setHeader('Access-Control-Allow-Origin', "*");

    // Request methods you wish to allow
    res.setHeader('Access-Control-Allow-Methods', 'GET, POST, OPTIONS, PUT, PATCH, DELETE');

    // Request headers you wish to allow
    res.setHeader('Access-Control-Allow-Headers', 'X-Requested-With,content-type');

    // Set to true if you need the website to include cookies in the requests sent
    // to the API (e.g. in case you use sessions)
    res.setHeader('Access-Control-Allow-Credentials', true);

    // Pass to next layer of middleware
    next();
});

app.get('/:collection/count',function(req, res){
    console.log(':collection = ' + req.params.collection);
    
    MongoClient.connect(db_connection_string, function(err, db) {
        assert.equal(null, err);
        console.log("Connected correctly to server");
        
        db.collection(req.params.collection).count(function(err, count) {
            var result = {
                "collection": req.params.collection,
                "count": count
            };
            res.json(result);
            db.close();
        });
    });
});

app.get('/:collection/find/:id',function(req, res){
    console.log(':collection = ' + req.params.collection , 'find ' + req.params.id);
    
    MongoClient.connect(db_connection_string, function(err, db) {
        assert.equal(null, err);
        console.log("Connected correctly to server");
        
        db.collection(req.params.collection).findOne({"_id": ObjectId(req.params.id)}, function(err, result) {
            if(err){
                console.log('err',err);
                res.json(err);
            }else{
                console.log('success',result);
                res.json(result);
            }
            db.close();
        });
    });
});

app.get('/:collection/list',function(req, res){
    console.log(':collection = ' + req.params.collection);
    
    MongoClient.connect(db_connection_string, function(err, db) {
        assert.equal(null, err);
        console.log("Connected correctly to server");
        
        db.collection(req.params.collection).find().toArray(function(err, result) {
            if(err){
                console.log('err',err);
                res.json(err);
            }else{
                console.log('success',result);
                res.json(result);
            }
            db.close();
        });
    });
});

app.get('/Content/find/index/:index',function(req, res){
    console.log('/Content/find/index/:index', req.params.index);
    
    MongoClient.connect(db_connection_string, function(err, db) {
        assert.equal(null, err);
        console.log("Connected correctly to server");
        
        db.collection('Content').findOne({"index": ObjectId(req.params.index)}, function(err, result) {
            if(err){
                console.log('err',err);
                res.json(err);
            }else{
                console.log('success',result);
                res.json(result);
            }
            db.close();
        });
    });
});

app.post('/Content/post/:index',function(req, res){
    console.log('/Content/post/:index', req.params.index);
    
    MongoClient.connect(db_connection_string, function(err, db) {
        assert.equal(null, err);
        console.log("Connected correctly to server");
        
        async.waterfall([
            function check_payload(cb){
                var payload = {
                    "language": "繁體中文",
                    "index": ObjectId(req.params.index),
                    "content": req.body.content
                };
                if(req.body.language){
                    payload.language = req.body.language;
                }
                console.log('payload', payload);
                cb(null, payload);
            },
            function check_index(payload, cb){
                if(payload.index){
                    db.collection('Index').findOne({"_id": payload.index}, function(err, relatedIndex) {
                        if(err){
                            res.status(500);
                            cb(err);
                        }else{
                            if(relatedIndex){
                                cb(null, payload);
                            }else{
                                res.status(400);
                                cb({"error":"index " + req.params.index + " does not exist."});
                            }
                        }
                    });
                }else{
                    cb(null, payload);
                }
            }
        ], function done(err, payload){
            if(err){
                res.json(err);
            }else{
                db.collection('Content').insert(payload, function(err, result) {
                    if(err){
                        console.log('err',err);
                        res.json(err);
                    }else{
                        console.log('success',result);
                        res.json(result);
                    }
                    db.close();
                });
            }
        });
    });
});

app.put('/Content/update/:id',function(req, res){
    console.log('/Content/post/:id', req.params.id);
    
    MongoClient.connect(db_connection_string, function(err, db) {
        assert.equal(null, err);
        console.log("Connected correctly to server");
        
        async.waterfall([
            function check_payload(cb){
                var payload = {
                    "content": req.body.content
                };
                console.log('content', req.body.content);
                cb(null, payload);
            },
            function check_content(payload, cb){
                if(payload.content){
                    db.collection('Content').findOne({"_id": ObjectId(req.params.id)}, function(err, relatedContent) {
                        if(err){
                            res.status(500);
                            cb(err);
                        }else{
                            if(relatedContent){
                                cb(null, payload);
                            }else{
                                res.status(400);
                                cb({"error":"content " + req.params.id + " does not exist."});
                            }
                        }
                    });
                }else{
                    cb(null, payload);
                }
            }
        ], function done(err, payload){
            if(err){
                res.json(err);
            }else{
                db.collection('Content').update({"_id": ObjectId(req.params.id)}, {"$set" : payload}, function(err, result) {
                    if(err){
                        console.log('err',err);
                        res.json(err);
                    }else{
                        console.log('success',result);
                        res.json(result);
                    }
                    db.close();
                });
            }
        });
    });
});

app.get('/Index/trace/id/:id',function(req, res){
    console.log('/Index/trace/name/:id', req.params.id);
    
    MongoClient.connect(db_connection_string, function(err, db) {
        assert.equal(null, err);
        console.log("Connected correctly to server");
        
        db.collection('Index').find().toArray(function(err, indices) {
            if(err){
                console.log('err',err);
                res.json(err);
            }else{
                var result;
                for(var i = 0; i < indices.length; i++){
                    if(indices[i]._id == req.params.id){
                        result = indices[i];
                        break;
                    }
                }
                if(result){
                    var target = result;
                    while(target.parent){
                        console.log('target', target, target.parent);
                        for(var i = 0; i < indices.length; i++){
                            if(indices[i]._id.toString() == target.parent.toString()){
                                target.parent = indices[i]
                                target = indices[i];
                                break;
                            }
                        }
                    }
                    return res.json(result);
                }else{
                    return res.json({});
                }
            }
            db.close();
        });
    });
});

app.post('/Index/post',function(req, res){
    console.log('/Index/post');
    
    MongoClient.connect(db_connection_string, function(err, db) {
        assert.equal(null, err);
        console.log("Connected correctly to server");
        
        async.waterfall([
            function check_payload(cb){
                var payload = {
                    "displayName": req.body.displayName
                };
                if(req.body.parent){
                    payload.parent = ObjectId(req.body.parent);
                }
                console.log('payload', payload);
                cb(null, payload);
            },
            function check_parent(payload, cb){
                if(payload.parent){
                    db.collection('Index').findOne({"_id": payload.parent}, function(err, relatedParent) {
                        if(err){
                            res.status(500);
                            cb(err);
                        }else{
                            if(relatedParent){
                                cb(null, payload);
                            }else{
                                res.status(400);
                                cb({"error":"parent " + req.body.parent + " does not exist."});
                            }
                        }
                    });
                }else{
                    cb(null, payload);
                }
            }
        ], function done(err, payload){
            if(err){
                res.json(err);
            }else{
                db.collection('Index').insert(payload, function(err, result) {
                    if(err){
                        console.log('err',err);
                        res.json(err);
                    }else{
                        console.log('success',result);
                        res.json(result);
                    }
                    db.close();
                });
            }
        });
    });
});